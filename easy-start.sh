#!/usr/bin/env bash

# Since starting dev environment will be faster
# Easy start script uses `dev` npm script to wake up
#  service and frontend

git clone https://gitlab.com/kucukkanat/vamp-assignment.git assignment
cd assignment
yarn install
yarn dev