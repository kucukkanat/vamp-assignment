import React, { useState, useEffect } from "react";
import Header from "@vamp/dashboard/src/components/Header";
import Api from "@vamp/dashboard/src/lib/api";
import PerformanceChart from "@vamp/dashboard/src/components/PerformanceChart";

const api = new Api("ws://localhost:3000");

export default class App extends React.Component<
  any,
  { data: Array<{ timestamp: number; label: string; value: number }> }
> {
  constructor(props: any) {
    super(props);
    this.state = {
      data: []
    };
  }
  componentDidMount() {
    api.on("message", event => {
      const appended = [...this.state.data, JSON.parse(event.data)]
      // To free up memory don't fill indefinitely
      if(appended.length > 5000) {
        appended.shift()
      }
      this.setState({ data: appended });
    });
  }
  render() {
    const {data} = this.state;
    
    return (
      <div>
        <Header />
        <PerformanceChart data={data} />
      </div>
    );
  }
}
