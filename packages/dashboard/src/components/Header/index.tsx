import React from "react"
import logo from "@vamp/dashboard/assets/logo.svg"
import "./styles.scss"

export default () => <div className="Header">
    <img className="Header--logo" src={logo} alt=""/>
    <span className="Header--title">Vamp Dashboard</span>
</div>