import React from "react";
import {
  AreaChart,
  Area,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
  ResponsiveContainer
} from "recharts";
import { ICPUStat } from "@vamp/types";

interface IPerformanceChartProps {
  data: Array<ICPUStat>;
}
export default class PerformanceChart extends React.Component<
  IPerformanceChartProps,
  { span: number }
> {
  constructor(props: IPerformanceChartProps) {
    super(props);
    this.state = {
      span: 5
    };
  }
  setSpan = (event: any) => {
    const { value } = event.target;
    this.setState({ span: value });
  };
  render() {
    let rawdata = this.props.data;
    const data = rawdata.filter(stat => {
      const seconds = Math.round(stat.timestamp / 1000)
      return seconds % this.state.span === 0
    })
    return (
      <div style={{ width: "100%", height: 300 }}>
        <ResponsiveContainer>
          <AreaChart
            width={500}
            height={300}
            data={data}
            margin={{
              top: 5,
              right: 20,
              left: 10,
              bottom: 5
            }}
          >
            <CartesianGrid strokeDasharray="9 3" />
            <XAxis dataKey="label" />
            <YAxis />
            <Tooltip />
            <Legend />
            <Area
              isAnimationActive={false}
              type="linear"
              dataKey="value"
              stroke="#8884d8"
              activeDot={{ r: 8 }}
            />
          </AreaChart>
        </ResponsiveContainer>
        <div style={{padding:"0px 20px"}}>
          <h3 className="text-center">{this.state.span} seconds</h3>
          <select onChange={this.setSpan} value={this.state.span} className="form-control">
            <option value={1}>1 second</option>
            <option value={2}>2 seconds</option>
            <option value={5}>5 seconds</option>
            <option value={15}>15 seconds</option>
            <option value={30}>30 seconds</option>
          </select>
        </div>
      </div>
    );
  }
}
