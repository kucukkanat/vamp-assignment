import React from "react"
import ReactDOM from "react-dom"
import App from "./App"
// Make sure our styles behave the same on all browsers
import "normalize.css";
import "@vamp/dashboard/styles/global.scss"

const root = document.getElementById("root")

ReactDOM.render(<App/>, root)
