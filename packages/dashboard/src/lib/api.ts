import Mitt from "mitt"
export default class Api extends Mitt {
    socket: any
    constructor(path:string) {
        super()
        this.socket = new WebSocket(path)
        this.socket.onopen = (event:any) => this.emit("open", event)
        this.socket.onmessage = (event:any) => this.emit("message", event)
    }
    close = () => {
        this.socket.close()
    }
    send = (message:object) => {
        this.socket.send(JSON.stringify(message))
    }
}