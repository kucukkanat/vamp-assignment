const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const WebpackMonitor = require('webpack-monitor');
const webpack = require('webpack');

module.exports = {
  entry: "./src/index.tsx",
  output: {
    filename: "bundle.js",
    path: path.resolve(__dirname, "dist"),
    libraryTarget: "umd"
  },
  module: {
    rules: [
      {
        test: /\.(t|j)sx?$/,
        exclude: /node_modules/,
        use: {
          loader: "ts-loader",
          options: {
              transpileOnly: false
          }
        }
      },
      {
        test: /\.s?css$/,
        use: [
          {
            loader: "style-loader",
            options: {
              sourceMap: true
            }
          },
          {
            loader: "css-loader",
            options: {
              sourceMap: true
            }
          },
          {
            loader: "sass-loader",
            options: {
              sourceMap: true
            }
          }
        ]
      },
      {
        test: /\.(png|jpg|gif|svg)$/i,
        use: [
          {
            loader: "url-loader",
            options: {
              limit: 8192
            }
          }
        ]
      }
    ]
  },
  devtool: "source-map",
  target: "web",
  externals: [],
  devServer: {
    host: "0.0.0.0",
    port: 8080,
    historyApiFallback: true,
    compress: true,
    // The bundled files will be available in the 'browser' under this path.
    // html output and bundle outputs
    publicPath: "/",
    hot: true,
    proxy: {
      "/cpu-stats": "http://localhost:3000"
    }
  },
  resolve: {
    // Webpack does not resolve .tsx? files if not declared here
    extensions: [".js", ".jsx", ".json", ".ts", ".tsx", ".mjs"]
  },
  plugins: [
    new HtmlWebpackPlugin({
      filename: "index.html",
      template: path.resolve(__dirname, "src/index.html")
    }),
    new webpack.HotModuleReplacementPlugin(),
    // new WebpackMonitor({
    //   capture: true, // -> default 'true'
    //   target: path.resolve(__dirname,'./monitor.json'), // default -> '../monitor/stats.json'
    //   launch: true, // -> default 'false'
    //   port: 8081, // default -> 8081
    // }),
  ]
};
