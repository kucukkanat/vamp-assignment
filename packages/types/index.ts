export interface ICPUStat {
    timestamp: number
    label: string
    value: number
}

