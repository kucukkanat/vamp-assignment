import moment from "moment";
import { ICPUStat } from "@vamp/types";
export const THRESHOLD = 5

/**
 * Generates a random stat object relative to the stat value given
 * between 0-100
 * @param {number} lastStat - Relative stat
 * @param {boolean} increasing - Should increase ot nor
 * @returns {ICPUStat} randomStat 
 */
export const generateStat = (lastStat:number, increasing?: boolean): ICPUStat => {
  const date = new Date()
  const random = Math.round(Math.random() * THRESHOLD)
  let value = increasing ? lastStat + random : lastStat - random
  // Make sure percentage is between 0-100
  value = value < 0 ? 0 : value
  value = value > 100 ? 100 : value
  return {
    timestamp: date.getTime(),
    label: moment(date).format("HH:mm:ss"),
    value
  };
};

/**
 * @param cb - Callback function to be called when there is a new stat
 * @param timeSpan - Time interval as seconds
 */
export const generator = (cb: (stat: ICPUStat) => void, timeSpan: number): any[] => {
  let isIncreasing = false
  let lastStat = 50
  
  // Flip a coin to increase or decrease every 10 seconds
   const coinFlip = setInterval(()=>{
      isIncreasing = Math.random() > 0.5
  },10000)

  let interval = setInterval(() => {
    const data = generateStat(lastStat,isIncreasing);
    lastStat = data.value
    cb(data);
  }, timeSpan * 1000);

  return [interval,coinFlip];
};
