import { generateStat, THRESHOLD, generator } from "./generator";
import { ICPUStat } from "@vamp/types";

describe("Generator tests", () => {
  test("Stat generator creates a stat object with expected properties", () => {
    const stat = generateStat(30);
    expect(stat).toHaveProperty("timestamp");
    expect(stat).toHaveProperty("label");
    expect(stat).toHaveProperty("value");
    expect(typeof stat.timestamp).toBe("number");
  });
  test(`Stat generator should not go outside the boundaries of +-${THRESHOLD} relatively from the lastStat`, () => {
    const testValue = 30;
    const { value } = generateStat(testValue);
    expect(value).toBeLessThanOrEqual(testValue + THRESHOLD);
    expect(value).toBeGreaterThanOrEqual(testValue - THRESHOLD);
  });

  test(`Stat generator generates 3 stats in 3.5 seconds, given 1 second interval as a parameter`, done => {
    const stats: ICPUStat[] = [];
    const [generatorInterval, coinFlipInterval] = generator(stat => {
      stats.push(stat);
    }, 1);

    // Stop after 3 seconds
    setTimeout(() => {
      clearTimeout(generatorInterval);
      clearTimeout(coinFlipInterval);
      expect(stats.length).toBe(3);
      done();
    }, 3500);
  });
});
