import WebSocket from 'ws';
import Mitt from "mitt"
import {generator} from "./generator"
 
const bus = new Mitt()
const wss = new WebSocket.Server({ port: 3000 });

// Start generating fake stats
generator((stat)=>{
  bus.emit("cpu-stat", stat)
}, 1)

wss.on('connection', function connection(ws:any) {
  // ws.on('message', function incoming(message:any) {
  //   console.log('received: %s', message);
  // });
  bus.on("cpu-stat", stat => {
    ws.send(JSON.stringify(stat));
  })
});
