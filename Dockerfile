# Use an official node:alpine image. (Alpine is to have a lightweight image)
FROM node:alpine

# Set the working directory to /app
WORKDIR /app

# Copy the current directory contents into the container at /app
COPY ./ /app

# Install any needed packages specified 
RUN yarn install
RUN yarn build

# Run app.py when the container launches
CMD ["yarn", "start"]