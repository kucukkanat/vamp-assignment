# Vamp.io Frontend assignment

<center><img src="packages/readme_logo.png" alt="Readme Logo" /></center>

## Table of contents

- [Requirements](#requirements)
- [Installation](#installation)
- [Dependencies & Opinions](#dependencies)
- [Contact](#contact)

## Requirements
- [NodeJS](https://nodejs.org)
- [Yarn](https://yarnpkg.com/)
- [Docker](http://docker.com) - (Optional)
- [Docker Compose](https://docs.docker.com/compose) - (Optional)


## Installation

You can kickstart the project in 4 different ways : 

#### Easy Start!

You can easily start the service and wake up the frontend with this one liner : 

```
curl https://gitlab.com/kucukkanat/vamp-assignment/raw/master/easy-start.sh | bash
```

#### Only NodeJS - Yarn 

- Clone the repo
- Install dependencies
- Run dev servers

```
git clone https://gitlab.com/kucukkanat/vamp-assignment.git && cd vamp-assignment
yarn install 
yarn dev
```

#### Docker

- Clone the repo
- Build the image
- Run container

```
git clone https://gitlab.com/kucukkanat/vamp-assignment.git && cd vamp-assignment
docker build -t dashboard .
docker run -it --rm -p 8080:8080 -p 3000:3000 dashboard
```

#### Docker Compose

- Clone the repo
- Let docker compose spin the container(s) for you

```
git clone https://gitlab.com/kucukkanat/vamp-assignment.git && cd vamp-assignment
docker-compose up
```

| Make sure your ports `8080` and `3000` are free !

## Dependencies & Opinions

All commits are signed using [GPG](https://gnupg.org/) keys. (With some exceptions using another machine for a couple of commits)

I used [husky](https://github.com/typicode/husky) to have git hooks across dev machines to ensure code quality. Currently only runs [jest](https://jestjs.io/) to make sure all test are passing before a commit is added on top of git history. Also lets us keep a cleaner git history. 

[Yarn workspaces](https://yarnpkg.com/lang/en/docs/workspaces/) is used along with [lerna](https://github.com/lerna/lerna) following a [monorepo](https://en.wikipedia.org/wiki/Monorepo) strategy. This strategy helps developers to keep an integrity among packages and publish different single purpose building blocks of the application. 

[Typescript](https://typescriptlang.org) is used to fail earlier in build time rather than relying on runtime checks and more reliable scalability. Also brings much convenience to developer environment with autocompletion and other IDE features.

Sass is preferred over [styled-components](https://www.styled-components.com/) since we don't have cross platform concerns. If only developing with react for a web environment, allows developer better sourcemap support and code splitting with [extract-loader](https://www.npmjs.com/package/extract-loader) if needed.

Instead of create-react-app, to reflect my experience with build systems, configured webpack manually. The project could also be built with other build systems / bundlers such as Parcel or Rollup. As being the most common [webpack](https://webpack.js.org) is preferred. 

For charts [recharts](http://recharts.org/) library for react is used. If bundle size and slow 3G  mobile networks are not a concern recharts can be a reliable library for further development with a nice supportive community on github having over 10K stars and 100 contributors. 

While picking dependencies, github issue lifespan, number of contributors and generality of the library/framework/tool is taken into consideration.

## Contact

Hilmi Tolga SAHIN

[Linkedin](https://www.linkedin.com/in/hilmi-tolga-%C5%9Fahin-73664937/)

[Email](mailto:htolgasahin@gmail.com)

[Github](https://github.com/kucukkanat)

[Twitter](https://twitter.com/kucukkanat)